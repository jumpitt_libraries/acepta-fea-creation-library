package com.jumpitt.aceptafeacreationprocess;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jumpitt.fea_creation.ActivationActivity;
import com.jumpitt.fea_library.FEAClient;
import com.jumpitt.fea_library.FEAListener;
import com.jumpitt.fea_library.model.Auth;
import com.jumpitt.fea_sign.DocumentAuthorizationActivity;
import com.jumpitt.fea_sign.FEAFingerprintHelper;
import com.jumpitt.fea_sign.SignSuccessActivity;

import static com.jumpitt.fea_creation.ActivationActivity.REQUEST_ACTIVATION;

//import com.jumpitt.aceptafealibrary.VerificationActivity;

//import static com.jumpitt.aceptafealibrary.VerificationActivity.REQUEST_PHONE_VERIFICATION;


public class MainActivity extends AppCompatActivity {
    private TextView status;
    private boolean feaInitialized = false;
    private ProgressBar progress;
    private BottomSheetDialogFragment mRegisterFingerDialog;
    private Button registerFinger;
    private Button retrieveFEA;
    private FEAFingerprintHelper mFingerPrintHelper;
    public static final int REQUEST_SUCCESS = 320;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Agregar estas línea en la clase application
        FEAClient.init(this);
        FEAFingerprintHelper.init(this);

        status = findViewById(R.id.estado);
        progress = findViewById(R.id.progressbar);

        progress.setVisibility(View.VISIBLE);

        FEAClient feaClient = FEAClient.getInstance();
        mFingerPrintHelper = FEAFingerprintHelper.getInstance();

        feaClient.authenticate("1234", new FEAListener<Auth>() {
            @Override
            public void onSuccess(Auth auth) {
                progress.setVisibility(View.GONE);
                if (auth != null) {
                    Log.d("TOKENNN", auth.getToken());
                    status.setText("FEA DISPONIBLE");
                    feaInitialized = true;
                } else {
                    status.setText("FEA NO DISPONIBLE");
                    feaInitialized = false;
                }
            }

            @Override
            public void onError(Throwable throwable) {
                progress.setVisibility(View.GONE);
                status.setText("ERROR AL DISPONIBILIZAR LA FEA");
                feaInitialized = false;
            }
        });

        Button button = findViewById(R.id.verification);
        Button button2 = findViewById(R.id.creation);
        registerFinger = findViewById(R.id.registrate);
        retrieveFEA = findViewById(R.id.retrieve);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(MainActivity.this, VerificationActivity.class);
                //startActivityForResult(intent, REQUEST_PHONE_VERIFICATION);
                //Intent intent = new Intent(MainActivity.this, SignInstanceActivity.class);
                //startActivityForResult(intent, 9999);
                //FEAClient.clearData();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feaInitialized) {
                    Intent intent = new Intent(MainActivity.this, ActivationActivity.class);
                    intent.putExtra("ANIMATE", false);
                    intent.putExtra("MOBILE_ID", "1234");
                    startActivityForResult(intent, REQUEST_ACTIVATION);
                }
            }
        });

        registerFinger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DocumentAuthorizationActivity.class);
                intent.putExtra(DocumentAuthorizationActivity.NO_PREVIEW, true);
                //intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_ID, "32");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_CODE, "32");
                intent.putExtra(DocumentAuthorizationActivity.DNI, "17162369-3");
                //intent.putExtra(DocumentAuthorizationActivity.SOCKET_ID, "12");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_CREATED_AT, "02/04/2019 07:49:54");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_NAME, "Documento de prueba");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_URL, "http://www.africau.edu/images/default/sample.pdf");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_INSTITUTION, "Scotiabank");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_INSTITUTION_LOGO, "http://imagenpng.com/wp-content/uploads/2016/09/fanta.jpg");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_STATE, "PENDIENTE");
                startActivityForResult(intent, 9999);
                /*if (mRegisterFingerDialog == null) {
                    mRegisterFingerDialog = FEARegisterFingerDialog.newInstance(new FEARegisterFingerDialog.OnFingerprintRegisterListener() {
                        @Override
                        public void registered(boolean status) {
                            retrieveFEA.setEnabled(status);
                            if (status) registerFinger.setEnabled(false);
                        }
                    });
                }

                mRegisterFingerDialog.show(getSupportFragmentManager(), "Register");*/
            }
        });

        retrieveFEA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Success Test
                /*
                int type = 1;
                Intent intent = new Intent(getApplicationContext(), SignSuccessActivity.class);
                intent.putExtra(SignSuccessActivity.EXTRA_LOGO, (type == 0) ? R.drawable.ic_sign_success_logo : R.drawable.ic_sign_reject_logo);
                intent.putExtra(SignSuccessActivity.EXTRA_TITLE, type == 0 ? getString(R.string.fls_sign_document_success_title) : getString(R.string.fls_reject_document_success_title));
                intent.putExtra(SignSuccessActivity.EXTRA_MESSAGE, type == 0 ? getString(R.string.fls_sign_document_success_message) : getString(R.string.fls_reject_document_success_message));
                intent.putExtra(SignSuccessActivity.EXTRA_BUTTON_TEXT, getString(R.string.fls_success_next_button_text));
                intent.putExtra(SignSuccessActivity.EXTRA_TYPE, SignSuccessActivity.TYPE_SIGN);
                startActivityForResult(intent, REQUEST_SUCCESS);*/


                Intent intent = new Intent(MainActivity.this, DocumentAuthorizationActivity.class);
                //intent.putExtra(DocumentAuthorizationActivity.NO_PREVIEW, true);
                //intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_ID, "32");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_CODE, "32");
                intent.putExtra(DocumentAuthorizationActivity.DNI, "17162369-3");
                //intent.putExtra(DocumentAuthorizationActivity.SOCKET_ID, "12");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_CREATED_AT, "02/04/2019 07:49:54");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_NAME, "Documento de prueba");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_URL, "https://5.dec.cl/document/previewQR/W2800000E60CA31MA2/6f07bb7830bcb67e46fd0c84ce14a9e8130a414b");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_INSTITUTION, "Scotiabank");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_INSTITUTION_LOGO, "https://preprod.acepta.com/Fintech/img/logo_propius.png");
                intent.putExtra(DocumentAuthorizationActivity.DOCUMENT_STATE, "PENDIENTE");
                startActivityForResult(intent, 9999);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ACTIVATION && resultCode == RESULT_OK) {
            if (FEAFingerprintHelper.isAvailable()) {
                createDialog("HUELLA DISPONIBLE", "DESEA FIRMAR CON SU HUELLA?", "SI", "NO").onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        registerFinger.performClick();
                    }
                }).show();
            }
        }
        //if (requestCode == REQUEST_PHONE_VERIFICATION) {
        //Toast.makeText(this, (resultCode == RESULT_OK) ? "Teléfono verificado: " + data.getExtras().get("PHONE_NUMBER") : "Error", Toast.LENGTH_SHORT).show();
        //}
    }


    private MaterialDialog.Builder createDialog(String title, String content, String positive, String negative) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .typeface(ResourcesCompat.getFont(this, R.font.bold), ResourcesCompat.getFont(this, R.font.regular))
                .negativeColor(getResources().getColor(R.color.text_color_black))
                .cancelable(false)
                .canceledOnTouchOutside(false);
        if (title != null)
            builder.title(title);
        if (content != null)
            builder.content(content);
        if (negative != null)
            builder.negativeText(negative);
        if (positive != null)
            builder.positiveText(positive);

        return builder;
    }
}
