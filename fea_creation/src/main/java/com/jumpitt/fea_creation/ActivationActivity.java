package com.jumpitt.fea_creation;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.view.View;

import java.util.Objects;

import static com.jumpitt.fea_creation.Utils.hideKeyboard;

/**
 * Created by felipe on 09-02-18.
 */

public class ActivationActivity extends BaseActivity implements FragmentManager.OnBackStackChangedListener, ActivationContract.View {
    public static final int REQUEST_ACTIVATION = 10002;
    public static final int FRAGMENT_REQUEST = 0;
    public static final int FRAGMENT_ACTIVATION = 1;
    private final String TAG = ActivationActivity.class.getSimpleName();
    ConstraintLayout root;
    View layer;
    Toolbar toolbar;
    FragmentManager fm;
    ConstraintSet initialConstraints;
    private int actualFragment;
    private String requestNumber;
    private String pin;
    private String phone;
    private boolean animate = true;
    private boolean loading = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            phone = extras.getString("MOBILE_ID");
            animate = extras.getBoolean("ANIMATE");
        } else {
            phone = "UNDEFINED";
            animate = true;
        }
        if (animate) {
            setContentView(R.layout.activity_verification_initial);
        } else {
            setContentView(R.layout.activity_verification);
        }

        root = findViewById(R.id.coordinator_layout);
        layer = findViewById(R.id.layer);
        toolbar = findViewById(R.id.toolbar);

        setupToolbar(toolbar);

        initialConstraints = new ConstraintSet();
        initialConstraints.clone(this, R.layout.activity_verification_initial); //Backup
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showInitialFragment();
            }
        }, animate ? 500 : 0);

    }

    private void showInitialFragment() {
        fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() == 0) {
            showFragment(FRAGMENT_REQUEST);
            fm.addOnBackStackChangedListener(ActivationActivity.this);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (animate) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ConstraintSet finalConstraints = new ConstraintSet();
                    finalConstraints.clone(ActivationActivity.this, R.layout.activity_verification);
                    TransitionManager.beginDelayedTransition(root);
                    finalConstraints.applyTo(root);
                }
            }, 200);
        }
    }


    private void showFragment(int fragment) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right);
        if (fragment == 0) {
            Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.flc_req_number_toolbar_title_text));
            ft.replace(R.id.fragment_container, new RequestNumberFragment()).addToBackStack(null);
        } else {
            Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.flc_activation_code_toolbar_title_text));
            ft.replace(R.id.fragment_container, new ActivationCodeFragment()).addToBackStack(null);
        }
        ft.commit();
        loading = false;
    }

    @Override
    protected void onDestroy() {
        fm.removeOnBackStackChangedListener(this);
        super.onDestroy();
    }

    @Override
    public void onBackStackChanged() {
        if (fm.getBackStackEntryCount() == 0) {
            return;
        }
        actualFragment = fm.getBackStackEntryCount() - 1;
    }

    @Override
    public void onBackPressed() {
        if (actualFragment == FRAGMENT_REQUEST) {
            if (animate) {
                fm.beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_right)
                        .remove(fm.getFragments().get(0))
                        .commit();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TransitionManager.beginDelayedTransition(root);
                        initialConstraints.applyTo(root);
                    }
                }, 300);
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    supportFinishAfterTransition();
                }
            }, animate ? 500 : 0);
        } else {
            Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.flc_req_number_toolbar_title_text));
            super.onBackPressed();
        }
    }

    @Override
    public void updateRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    @Override
    public void updatePin(String pin) {
        this.pin = pin;
    }

    @Override
    public void onNextButtonPressed() {
            if (actualFragment == FRAGMENT_ACTIVATION) {
                if (animate) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //layer.setBackgroundColor(textColorAccent);
                            TransitionManager.beginDelayedTransition(root);
                            initialConstraints.applyTo(root);
                        }
                    }, 300);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(ActivationActivity.this, FEAActivity.class);
                        intent.putExtra("RC", requestNumber);
                        intent.putExtra("PIN", pin);
                        intent.putExtra("MOBILE_ID", phone);
                        startActivityForResult(intent, 0);
                    }
                }, animate ? 500 : 0);
            } else {
                hideKeyboard(root);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showFragment(FRAGMENT_ACTIVATION);
                    }
                }, 200);
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(resultCode);
            finish();
        }
    }
}
