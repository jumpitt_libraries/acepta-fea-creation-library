package com.jumpitt.fea_creation;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class ActivationCodeFragment extends Fragment {

    TextInputEditText activationInput;
    private ActivationContract.View activityListener;
    private MaterialButton nextButtonActCode;
    private ColorStateList buttonDisableColor;
    private ColorStateList buttonEnableColor;
    private RelativeLayout inforelativeLayout;
    private static final String TAG = ActivationCodeFragment.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_activation_code_content, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activityListener = (ActivationContract.View) getActivity();

        buttonDisableColor = ContextCompat.getColorStateList(view.getContext(), R.color.flc_button_disable_color);
        buttonEnableColor = ContextCompat.getColorStateList(view.getContext(), R.color.flc_button_color);
        nextButtonActCode = view.findViewById(R.id.next_button_activation_code);
        inforelativeLayout = view.findViewById(R.id.relativeLayout);

        activationInput = view.findViewById(R.id.activation_input_edit_text);
        toggleMaterialButton(activationInput.getText().length() > 0);

        activationInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onPhoneTextChange(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        nextButtonActCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButtonActCode.setEnabled(false );
                activityListener.onNextButtonPressed();
            }
        });

        String message = getString(R.string.flc_act_code_info_message);
        if (!message.equals("")) {
            inforelativeLayout.setVisibility(View.VISIBLE);
        } else {
            inforelativeLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        toggleMaterialButton(activationInput.getText().length() > 0);
    }

    void onPhoneTextChange(CharSequence c) {
        toggleMaterialButton(activationInput.getText().length() > 0);
        activityListener.updatePin(activationInput.getText().toString());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        toggleMaterialButton(activationInput.getText().length() > 0);
    }

    private void toggleMaterialButton(boolean b) {
        if (nextButtonActCode == null) return;
        nextButtonActCode.setBackgroundTintList(b ? buttonEnableColor : buttonDisableColor);
        nextButtonActCode.setEnabled(b);
    }
}
