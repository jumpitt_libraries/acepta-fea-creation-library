package com.jumpitt.fea_creation;

/**
 * Created by felipe on 09-02-18.
 */

interface ActivationContract {
    interface View {

        void updateRequestNumber(String requestNumber);

        void updatePin(String pin);

        void onNextButtonPressed();
    }
}
