package com.jumpitt.fea_creation;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.button.MaterialButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by felipe on 01-02-18.
 */

public class CreateSuccessActivity extends AppCompatActivity {
    public static final String EXTRA_LOGO = "extraLogo";
    public static final String EXTRA_TITLE = "extraTitle";
    public static final String EXTRA_MESSAGE = "extraMessage";
    public static final String EXTRA_BUTTON_TEXT = "extraButtonText";
    public static final String EXTRA_TYPE = "extraType";
    public static final int TYPE_VERIFICATION = 1001;
    public static final int TYPE_ACTIVATION = 1002;
    public static final int TYPE_SIGN = 1003;
    public static final String TAG = CreateSuccessActivity.class.getSimpleName();

    ConstraintLayout root;
    ImageView logo;
    AppCompatTextView title;
    AppCompatTextView message;
    MaterialButton button;
    int type = TYPE_ACTIVATION;
    String phone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation_success);

        Log.i(TAG, "Entro a success de Creation");

        root = findViewById(R.id.create_container);
        logo = findViewById(R.id.logo);
        title = findViewById(R.id.title);
        message = findViewById(R.id.message);
        button = findViewById(R.id.continue_button);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int drawable = extras.getInt(EXTRA_LOGO);
            logo.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawable, null));
            title.setText(extras.getString(EXTRA_TITLE));
            message.setText(extras.getString(EXTRA_MESSAGE));
            button.setText(extras.getString(EXTRA_BUTTON_TEXT));
            type = extras.getInt(EXTRA_TYPE);
            phone = extras.getString("PHONE");
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onContinueButtonClicked();
            }
        });
    }

    void onContinueButtonClicked() {
        button.setEnabled(false);
        switch (type) {
            case TYPE_VERIFICATION:
                Intent intent = new Intent(this, ActivationActivity.class);
                intent.putExtra("PHONE", phone);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, root, root.getTransitionName());
                    startActivity(intent, activityOptionsCompat.toBundle());
                } else {
                    startActivity(intent);
                    finishAffinity();
                }
                break;
            case TYPE_ACTIVATION:
                setResult(RESULT_OK);
                finish();
                /*startActivity(new Intent(this, HomeActivity.class));
                finishAffinity();*/
                break;
            case TYPE_SIGN:
                setResult(RESULT_OK);
                finish();
                break;
            default:
                Log.w(getString(R.string.app_name), "Unknown Success Type");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        switch (type) {
            case TYPE_SIGN:
                setResult(RESULT_OK);
                super.onBackPressed();
                break;
            case TYPE_ACTIVATION:
                break;
            default:
                new AlertDialog.Builder(this)
                        .setTitle("¿Desea Salir?")
                        .setMessage("Si sale perderá todo el progreso.")
                        .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
                break;
        }
    }
}
