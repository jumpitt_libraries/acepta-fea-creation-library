package com.jumpitt.fea_creation;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

/**
 * Created by felipe on 01-02-18.
 */

public class FEAActivity extends BaseActivity {

    private Toolbar toolbar;
    private MaterialButton activateButton;
    private TextInputEditText feaInput;
    private TextInputEditText repeatInput;
    private boolean passValid = false;
    private boolean repeatOK = false;
    private ColorStateList buttonDisableColor;
    private ColorStateList buttonEnableColor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fea);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.flc_sign_pass_toolbar_title_text));
        activateButton = findViewById(R.id.activate_button);
        feaInput = findViewById(R.id.pass_fea_input_edit_text);
        repeatInput = findViewById(R.id.repeat_pass_fea_input_edit_text);
        buttonDisableColor = ContextCompat.getColorStateList(this, R.color.flc_active_sign_button_disable_color);
        buttonEnableColor = ContextCompat.getColorStateList(this, R.color.flc_active_sign_button_color);

        activateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onActivateClicked();
            }
        });

        feaInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onFeaTextChange(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        repeatInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onRepeatTextChange(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        setupToolbar(toolbar);
        toggleMaterialButton(passValid && repeatOK);
    }

    void onActivateClicked() {
        toggleMaterialButton(false);
        Intent intent = new Intent(this, LoadingActivity.class);
        intent.putExtras(getIntent());
        intent.putExtra("FEA", feaInput.getText().toString());
        startActivityForResult(intent, 0);
    }

    void onFeaTextChange(CharSequence fea) {
        repeatInput.setText("");
        passValid = Utils.matchFEA(fea);
        //if (!passValid) {
        //    feaSuccess.setVisibility(View.GONE);
        //} else {
        //    feaSuccess.setVisibility(View.VISIBLE);
        //}
        //toggleButton();
        toggleMaterialButton(passValid && repeatOK);
    }

    void onRepeatTextChange(CharSequence repeat) {
        repeatOK = repeatInput.getText().length() > 0 && repeat.toString().equals(feaInput.getText().toString()) && Utils.matchFEA(feaInput.getText().toString());
        //if (!repeatOK) {
        //    repeatSuccess.setVisibility(View.GONE);
        //} else {
        //    repeatSuccess.setVisibility(View.VISIBLE);
        //}
        //toggleButton();
        toggleMaterialButton(passValid && repeatOK);
    }

    //private void toggleButton() {
    //    activateButton.setEnabled(passValid && repeatOK);
    //}

    private void toggleMaterialButton(boolean b) {
        if (activateButton == null) return;
        activateButton.setBackgroundTintList(b ? buttonEnableColor : buttonDisableColor);
        activateButton.setEnabled(b);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        toggleMaterialButton(passValid && repeatOK);
        if (resultCode == RESULT_OK) {
            setResult(resultCode);
            finish();
        }
    }
}
