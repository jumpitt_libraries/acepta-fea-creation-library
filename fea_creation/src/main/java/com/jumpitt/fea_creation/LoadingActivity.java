package com.jumpitt.fea_creation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by felipe on 01-02-18.
 */

public class LoadingActivity extends AppCompatActivity implements LoadingContract.View {

    private LoadingContract.Presenter presenter;
    private String fea;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activate_in_progress);

        presenter = new LoadingPresenter(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            fea = extras.getString("FEA");
            presenter.createCertificate(fea, extras.getString("PIN"), extras.getString("RC"), extras.getString("MOBILE_ID"));

            /*
            Intent intent = new Intent(this, CreateSuccessActivity.class);

            intent.putExtra(CreateSuccessActivity.EXTRA_LOGO, R.drawable.ic_fea_success);
            intent.putExtra(CreateSuccessActivity.EXTRA_TITLE, "Firma electrónica avanzada activada con éxito.");
            intent.putExtra(CreateSuccessActivity.EXTRA_MESSAGE, "Ya puedes comenzar a utilizarla y firmar documentos.");
            intent.putExtra(CreateSuccessActivity.EXTRA_BUTTON_TEXT, "Comenzar");
            intent.putExtra(CreateSuccessActivity.EXTRA_TYPE, CreateSuccessActivity.TYPE_ACTIVATION);
            startActivityForResult(intent, 0);
            */
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onSuccess() {
        //Hawk.put(HAWK_FEA, fea);
        Intent intent = new Intent(this, CreateSuccessActivity.class);
        intent.putExtra(CreateSuccessActivity.EXTRA_LOGO, R.drawable.ic_fea_success);
        intent.putExtra(CreateSuccessActivity.EXTRA_TITLE, "Firma electrónica avanzada activada con éxito.");
        intent.putExtra(CreateSuccessActivity.EXTRA_MESSAGE, "Ya puedes comenzar a utilizarla y firmar documentos.");
        intent.putExtra(CreateSuccessActivity.EXTRA_BUTTON_TEXT, "Comenzar");
        intent.putExtra(CreateSuccessActivity.EXTRA_TYPE, CreateSuccessActivity.TYPE_ACTIVATION);
        startActivityForResult(intent, 1234);
    }

    @Override
    public void onError(String glosa) {
        Toast.makeText(this, glosa, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setResult(resultCode);
        finish();
    }
}
