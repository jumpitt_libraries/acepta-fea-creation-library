package com.jumpitt.fea_creation;

public interface LoadingContract {
    interface View {

        void onSuccess();

        void onError(String glosa);
    }

    interface Presenter {

        void createCertificate(String fea, String pin, String rc, String phone);
    }
}
