package com.jumpitt.fea_creation;

import com.jumpitt.fea_library.FEAClient;
import com.jumpitt.fea_library.FEAListener;
import com.jumpitt.fea_library.model.Certificate;

public class LoadingPresenter implements LoadingContract.Presenter {
    private LoadingContract.View view;
    private FEAClient mFEAClient;

    public LoadingPresenter(LoadingContract.View view) {
        this.view = view;
        this.mFEAClient = FEAClient.getInstance();
    }

    @Override
    public void createCertificate(String fea, String pin, String rc, String phone) {
        mFEAClient.createCertificate(rc, pin, fea, phone, new FEAListener<Certificate>() {
            @Override
            public void onSuccess(Certificate certificate) {
                //Hawk.put(HAWK_USER, certificate.getDni());
                view.onSuccess();
            }

            @Override
            public void onError(Throwable throwable) {
                view.onError(throwable.getCause().getMessage());
            }
        });
    }
}
