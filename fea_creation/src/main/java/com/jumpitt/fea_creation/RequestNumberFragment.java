package com.jumpitt.fea_creation;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class RequestNumberFragment extends Fragment {

    TextInputEditText requestInput;
    private static final String SEPARATOR = "-";

    private ActivationContract.View activityListener;
    private MaterialButton nextButtonReqNumber;

    private final String TAG = RequestNumberFragment.class.getSimpleName();
    private ColorStateList buttonDisableColor;
    private ColorStateList buttonEnableColor;
    private RelativeLayout inforelativeLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_request_number_content, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activityListener = (ActivationContract.View) getActivity();

        buttonDisableColor = ContextCompat.getColorStateList(view.getContext(), R.color.flc_next_button_disable_color);
        buttonEnableColor = ContextCompat.getColorStateList(view.getContext(), R.color.flc_act_code_next_button_color);

        nextButtonReqNumber = view.findViewById(R.id.next_button_requ_number);
        requestInput = view.findViewById(R.id.request_input_edit_text);
        inforelativeLayout = view.findViewById(R.id.relativeLayout);
        toggleMaterialButton(requestInput.getText().length() == 20);

        requestInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                //TODO: pls improve this
                String clean = editable.toString().replace("-", "");
                String formatted = "";
                if (clean.length() > 14) {
                    formatted = clean.substring(0, 4) + SEPARATOR + clean.substring(4, 5) + SEPARATOR + clean.substring(5, 9) + SEPARATOR + clean.substring(9, 10) + SEPARATOR + clean.substring(10, 14) + SEPARATOR + clean.substring(14);
                } else if (clean.length() > 10) {
                    formatted = clean.substring(0, 4) + SEPARATOR + clean.substring(4, 5) + SEPARATOR + clean.substring(5, 9) + SEPARATOR + clean.substring(9, 10) + SEPARATOR + clean.substring(10);
                } else if (clean.length() > 9) {
                    formatted = clean.substring(0, 4) + SEPARATOR + clean.substring(4, 5) + SEPARATOR + clean.substring(5, 9) + SEPARATOR + clean.substring(9);
                } else if (clean.length() > 5) {
                    formatted = clean.substring(0, 4) + SEPARATOR + clean.substring(4, 5) + SEPARATOR + clean.substring(5);
                } else if (clean.length() > 4) {
                    formatted = clean.substring(0, 4) + SEPARATOR + clean.substring(4);
                } else {
                    formatted = clean;
                }
                requestInput.removeTextChangedListener(this);
                editable.replace(0, editable.length(), formatted.toUpperCase());
                requestInput.addTextChangedListener(this);
                onRequestTextChanged();
            }
        });
        nextButtonReqNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButtonReqNumber.setEnabled(false);
                activityListener.onNextButtonPressed();
            }
        });

        String message = getString(R.string.flc_req_number_info_message);
        if (!message.equals("")) {
            inforelativeLayout.setVisibility(View.VISIBLE);
        } else {
            inforelativeLayout.setVisibility(View.GONE);
        }

    }

    void onRequestTextChanged() {
        toggleMaterialButton(requestInput.getText().length() == 20);
        activityListener.updateRequestNumber(requestInput.getText().toString());
    }

    private void toggleMaterialButton(boolean b) {
        if (nextButtonReqNumber == null) return;
        nextButtonReqNumber.setBackgroundTintList(b ? buttonEnableColor : buttonDisableColor);
        nextButtonReqNumber.setEnabled(b);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        toggleMaterialButton(requestInput.getText().length() == 20);
    }
}
