package com.jumpitt.fea_creation;

import android.content.Context;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

class Utils {
    private static String C_INSTANCE2 = "RSA/ECB/PKCS1Padding";
    private static final String REGEX_FEA = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$";

    static void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    static boolean matchFEA(CharSequence fea) {
        Pattern pattern = Pattern.compile(REGEX_FEA);
        return pattern.matcher(fea).matches();
    }

    static String encrypt(String pkString, String message) {
        try {
            PublicKey pk = getKey(pkString);
            Cipher cipher = Cipher.getInstance(C_INSTANCE2);
            cipher.init(Cipher.ENCRYPT_MODE, pk);
            byte[] encryptedByteData = cipher.doFinal(message.getBytes("UTF-8"));

            return byteToHex(encryptedByteData);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    static String encrypt2(String module, String exp, String message) {
        try {
            BigInteger e = new BigInteger(1, new byte[]{1, 0, 0, 0, 1});
            BigInteger m = new BigInteger(1, hexStringToByteArray(module));
            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey pubKeyn = fact.generatePublic(keySpec);
            Cipher cipher = Cipher.getInstance(C_INSTANCE2);
            cipher.init(Cipher.ENCRYPT_MODE, pubKeyn);
            byte[] encryptedByteData = cipher.doFinal(message.getBytes());

            return byteToHex(encryptedByteData);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return null;
    }

    static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private static PublicKey getKey(String key) {
        try {
            byte[] byteKey = Base64.decode(key.getBytes(), Base64.DEFAULT);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");

            return kf.generatePublic(X509publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String byteToHex(byte[] origin) {
        try {
            StringBuilder buffer = new StringBuilder();
            for (int i = 0; i < origin.length; i++) {
                buffer.append(Character.forDigit((origin[i] >> 4) & 0xF, 16));
                buffer.append(Character.forDigit((origin[i] & 0xF), 16));
            }
            return buffer.toString();
        } catch (Exception ignore) {
        }
        return null;
    }
}
