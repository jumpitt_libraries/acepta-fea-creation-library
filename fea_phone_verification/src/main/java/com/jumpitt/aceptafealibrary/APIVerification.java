package com.jumpitt.aceptafealibrary;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

interface APIVerification {

    @POST("message/create")
    Call<CreateMessageResponse> create(@Body CreateMessageBody body);

    @POST("message/verify")
    Call<Void> verify(@Body VerifyMessageBody body);
}
