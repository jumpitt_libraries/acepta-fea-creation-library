package com.jumpitt.aceptafealibrary;

import com.google.gson.annotations.SerializedName;

class CreateMessageBody {
    @SerializedName("auth_token")
    private String token;
    private String phone;
    //private String provider;

    public CreateMessageBody(String token, String phone) {
        this.token = token;
        this.phone = phone;
        //this.provider = "twillio";
    }
}
