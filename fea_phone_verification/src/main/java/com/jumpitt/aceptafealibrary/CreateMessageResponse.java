package com.jumpitt.aceptafealibrary;

import com.google.gson.annotations.SerializedName;

class CreateMessageResponse {
    private OID oid;

    public String getOID() {
        if (oid == null) return null;
        return oid.id;
    }

    private class OID {
        @SerializedName("$oid")
        private String id;
    }
}
