package com.jumpitt.aceptafealibrary;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import static com.jumpitt.aceptafealibrary.Utils.hideKeyboard;

public class SendCodeFragment extends Fragment{
    private CardView firsNumberCard;
    private CardView secondNumberCard;
    private CardView thirdNumberCard;
    private CardView fourthNumberCard;
    private TextInputEditText firsNumberInput;
    private TextInputEditText secondNumberInput;
    private TextInputEditText thirdNumberInput;
    private TextInputEditText fourthNumberInput;
    private TextView phoneNumber;
    private VerificationContract.View activityListener;
    private int fieldNotEmpty = 0;

    public static SendCodeFragment newInstance(String phone) {

        Bundle args = new Bundle();
        args.putString("PHONE", phone);
        SendCodeFragment fragment = new SendCodeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppTheme_Code);
        LayoutInflater inflaterStyled = inflater.cloneInContext(contextThemeWrapper);
        return inflaterStyled.inflate(R.layout.fragment_send_code, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firsNumberCard = view.findViewById(R.id.first_number);
        secondNumberCard = view.findViewById(R.id.second_number);
        thirdNumberCard = view.findViewById(R.id.third_number);
        fourthNumberCard = view.findViewById(R.id.fourth_number);
        firsNumberInput = view.findViewById(R.id.first_number_input);
        secondNumberInput = view.findViewById(R.id.second_number_input);
        thirdNumberInput = view.findViewById(R.id.third_number_input);
        fourthNumberInput = view.findViewById(R.id.fourth_number_input);
        phoneNumber = view.findViewById(R.id.textView2);

        if (getArguments() != null) {
            String phone = getArguments().getString("PHONE");
            phoneNumber.setText(phone);
        }

        AppCompatTextView changeNumberButton = view.findViewById(R.id.change_number_button);
        changeNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityListener.changeNumber();
            }
        });

        AppCompatTextView resend = view.findViewById(R.id.resend_sms_button);
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityListener.resendCode();
            }
        });

        firsNumberInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onFirstTextChange(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        secondNumberInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onSecondTextChange(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        thirdNumberInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onThirdTextChange(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        fourthNumberInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onFourthTextChange(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        firsNumberInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                onFirstFocusChange(b);
            }
        });
        secondNumberInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                onSecondFocusChange(b);
            }
        });
        thirdNumberInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                onThirdFocusChange(b);
            }
        });
        fourthNumberInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                onFourthFocusChange(b);
            }
        });

        activityListener = (VerificationContract.View) getActivity();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        activityListener.updateMenuTitle(VerificationActivity.MENU_TITLE_NEXT);
        activityListener.toggleMenuItemEnable(fieldNotEmpty == 4);
    }

    void onFirstTextChange(CharSequence cs) {
        if (cs.length() > 0) {
            secondNumberInput.requestFocus();
            firsNumberCard.setCardElevation(6);
            fieldNotEmpty++;
        } else {
            fieldNotEmpty--;
        }
        activityListener.toggleMenuItemEnable(fieldNotEmpty == 4);
        if (fieldNotEmpty == 4) {
            String code = firsNumberInput.getText().toString() +
                    secondNumberInput.getText().toString() +
                    thirdNumberInput.getText().toString() +
                    fourthNumberInput.getText().toString();
            activityListener.updateCode(code);
        }

    }

    void onSecondTextChange(CharSequence cs) {
        if (cs.length() > 0) {
            thirdNumberInput.requestFocus();
            secondNumberCard.setCardElevation(6);
            fieldNotEmpty++;
        } else {
            fieldNotEmpty--;
        }
        activityListener.toggleMenuItemEnable(fieldNotEmpty == 4);
        if (fieldNotEmpty == 4) {
            String code = firsNumberInput.getText().toString() +
                    secondNumberInput.getText().toString() +
                    thirdNumberInput.getText().toString() +
                    fourthNumberInput.getText().toString();
            activityListener.updateCode(code);
        }
    }

    void onThirdTextChange(CharSequence cs) {
        if (cs.length() > 0) {
            fourthNumberInput.requestFocus();
            thirdNumberCard.setCardElevation(6);
            fieldNotEmpty++;
        } else {
            fieldNotEmpty--;
        }
        activityListener.toggleMenuItemEnable(fieldNotEmpty == 4);
        if (fieldNotEmpty == 4) {
            String code = firsNumberInput.getText().toString() +
                    secondNumberInput.getText().toString() +
                    thirdNumberInput.getText().toString() +
                    fourthNumberInput.getText().toString();
            activityListener.updateCode(code);
        }
    }

    void onFourthTextChange(CharSequence cs) {
        if (cs.length() > 0) {
            hideKeyboard(fourthNumberInput);
            fourthNumberInput.clearFocus();
            fourthNumberCard.setCardElevation(6);
            fieldNotEmpty++;
        } else {
            fieldNotEmpty--;
        }
        activityListener.toggleMenuItemEnable(fieldNotEmpty == 4);
        if (fieldNotEmpty == 4) {
            String code = firsNumberInput.getText().toString() +
                    secondNumberInput.getText().toString() +
                    thirdNumberInput.getText().toString() +
                    fourthNumberInput.getText().toString();
            activityListener.updateCode(code);
        }
    }

    void onFirstFocusChange(boolean focused) {
        if (firsNumberInput.getText().length() == 0) {
            firsNumberCard.setCardElevation((focused) ? 6 : 0);
        }
    }

    void onSecondFocusChange(boolean focused) {
        if (secondNumberInput.getText().length() == 0) {
            secondNumberCard.setCardElevation((focused) ? 6 : 0);
        }
    }

    void onThirdFocusChange(boolean focused) {
        if (thirdNumberInput.getText().length() == 0) {
            thirdNumberCard.setCardElevation((focused) ? 6 : 0);
        }
    }

    void onFourthFocusChange(boolean focused) {
        if (fourthNumberInput.getText().length() == 0) {
            fourthNumberCard.setCardElevation((focused) ? 6 : 0);
        }
    }
}
