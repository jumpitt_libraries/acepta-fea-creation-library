package com.jumpitt.aceptafealibrary;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SendPhoneFragment extends Fragment {
    AppCompatEditText phoneInput;
    AppCompatEditText codeInput;

    private VerificationContract.View activityListener;

    public static SendPhoneFragment newInstance() {

        Bundle args = new Bundle();

        SendPhoneFragment fragment = new SendPhoneFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_send_phone, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activityListener = (VerificationContract.View) getActivity();

        phoneInput = view.findViewById(R.id.phone_input);
        codeInput = view.findViewById(R.id.code);

        phoneInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onPhoneTextChange();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        codeInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onCodeTextChange();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String clean = editable.toString().replace("+", "");
                String code = "+" + clean;
                if (clean.length() > 0) {
                    codeInput.removeTextChangedListener(this);
                    editable.replace(0, editable.length(), code);
                    codeInput.addTextChangedListener(this);
                }
            }
        });
    }

    void onPhoneTextChange() {
        activityListener.toggleMenuItemEnable(phoneInput.getText().toString().length() == 9 && !codeInput.getText().toString().isEmpty());
        activityListener.updatePhone(codeInput.getText().toString() + phoneInput.getText().toString());
    }

    void onCodeTextChange() {
        activityListener.toggleMenuItemEnable(phoneInput.getText().toString().length() == 9 && !codeInput.getText().toString().isEmpty());
        activityListener.updatePhone(codeInput.getText().toString() + phoneInput.getText().toString());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        activityListener.updateMenuTitle(VerificationActivity.MENU_TITLE_SEND);
        activityListener.toggleMenuItemEnable(phoneInput.getText().length() == 8);
    }
}
