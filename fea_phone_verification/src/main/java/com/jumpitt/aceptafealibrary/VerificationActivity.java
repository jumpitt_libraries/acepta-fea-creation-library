package com.jumpitt.aceptafealibrary;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.transition.TransitionManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * Created by felipe on 09-02-18.
 */

public class VerificationActivity extends BaseActivity implements VerificationContract.View, FragmentManager.OnBackStackChangedListener {
    public static final int REQUEST_PHONE_VERIFICATION = 10001;
    public static final int FRAGMENT_PHONE = 0;
    public static final int FRAGMENT_CODE = 1;
    public static final String MENU_TITLE_SEND = "Enviar";
    public static final String MENU_TITLE_NEXT = "Siguiente";

    ConstraintLayout root;
    Toolbar toolbar;
    int textColorAccent;
    int textColorGrey;

    private int actualFragment;
    private FragmentManager fm;
    private MenuItem menuItem;
    private ConstraintSet initialConstraints;

    private VerificationPresenter presenter;
    private String phone;
    private String code;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_initial);

        root = findViewById(R.id.coordinator_layout);
        toolbar = findViewById(R.id.toolbar);
        textColorAccent = ContextCompat.getColor(this, R.color.text_color_accent);
        textColorGrey = ContextCompat.getColor(this, R.color.text_color_grey);

        setupToolbar(toolbar);

        presenter = new VerificationPresenter(this);

        initialConstraints = new ConstraintSet();
        initialConstraints.clone(this, R.layout.activity_verification_initial); //Backup
    }

    @Override
    protected void onStart() {
        super.onStart();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ConstraintSet finalConstraints = new ConstraintSet();
                finalConstraints.clone(VerificationActivity.this, R.layout.activity_verification);
                TransitionManager.beginDelayedTransition(root);
                finalConstraints.applyTo(root);
            }
        }, 200);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fm = getSupportFragmentManager();
                showFragment(FRAGMENT_PHONE);
                fm.addOnBackStackChangedListener(VerificationActivity.this);
            }
        }, 500);
    }

    private void showFragment(int fragment) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right);
        if (fragment == 0) {
            ft.replace(R.id.fragment_container, SendPhoneFragment.newInstance()).addToBackStack(null);
        } else {
            ft.replace(R.id.fragment_container, SendCodeFragment.newInstance(phone)).addToBackStack(null);
        }
        ft.commit();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.finish_verification_menu, menu);
        menuItem = menu.findItem(R.id.option_next);
        actualFragment = 0;
        menuItem.setTitle(MENU_TITLE_SEND);
        changeMenuState(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        fm.removeOnBackStackChangedListener(this);
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                if (item.getItemId() == R.id.option_next) {
                    if (actualFragment == FRAGMENT_CODE) {
                        presenter.sendCode(code);
                    } else {
                        presenter.sendPhone(phone, true);
                    }
                    return true;
                } else {
                    return super.onOptionsItemSelected(item);
                }
        }
    }

    @Override
    public void onBackStackChanged() {
        if (fm.getBackStackEntryCount() == 0 || menuItem == null) {
            return;
        }
        actualFragment = fm.getBackStackEntryCount() - 1;
    }

    @Override
    public void onBackPressed() {
        if (actualFragment == FRAGMENT_PHONE) {
            fm.beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_right)
                    .remove(fm.getFragments().get(0))
                    .commit();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    TransitionManager.beginDelayedTransition(root);
                    initialConstraints.applyTo(root);
                }
            }, 300);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    supportFinishAfterTransition();
                }
            }, 500);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void toggleMenuItemEnable(boolean b) {
        if (menuItem == null) return;
        changeMenuState(b);

    }

    @Override
    public void updateMenuTitle(String title) {
        if (menuItem == null) return;
        menuItem.setTitle(title);
    }

    @Override
    public void updatePhone(String phone) {
        this.phone = phone;
    }

    @Override
    public void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
    }

    @Override
    public void onPhoneSuccess() {
        showFragment(FRAGMENT_CODE);
    }

    @Override
    public void onError(int step, int errorCode, String message) {
        Toast.makeText(this, "Error: " + errorCode + " " + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(int step, Throwable cause) {
        Toast.makeText(this, "Failure ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateCode(String code) {
        this.code = code;
    }

    @Override
    public void onCodeSuccess() {
        Intent result = new Intent();
        result.putExtra("PHONE_NUMBER", phone);
        setResult(RESULT_OK, result);
        finish();
    }

    @Override
    public void changeNumber() {
        onBackPressed();
    }

    @Override
    public void resendCode() {
        presenter.sendPhone(phone, false);
    }

    private void changeMenuState(boolean b) {
        SpannableString s = new SpannableString(menuItem.getTitle());
        s.setSpan(new ForegroundColorSpan((b) ? textColorAccent : textColorGrey), 0, s.length(), 0);
        menuItem.setTitle(s);
        menuItem.setEnabled(b);
    }
}
