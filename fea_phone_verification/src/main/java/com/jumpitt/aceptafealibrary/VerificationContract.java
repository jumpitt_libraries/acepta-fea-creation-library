package com.jumpitt.aceptafealibrary;

/**
 * Created by felipe on 09-02-18.
 */

interface VerificationContract {
    interface Presenter {

        void sendPhone(String phone, boolean changeFragment);

        void onPhoneSent(CreateMessageResponse body);

        void sendCode(String code);

        void onCodeSent(boolean successful);
    }

    interface View {

        void toggleMenuItemEnable(boolean b);

        void updateMenuTitle(String title);

        void updatePhone(String phone);

        void showProgressDialog(String message);

        void dismissProgressDialog();

        void onPhoneSuccess();

        void onError(int step, int errorCode, String message);

        void onFailure(int step, Throwable cause);

        void showMessage(String message);

        void updateCode(String code);

        void onCodeSuccess();

        void changeNumber();

        void resendCode();
    }

    interface Interactor {

        void verifyPhone(String phone);

        void sendCode(String code, String oid);
    }
}
