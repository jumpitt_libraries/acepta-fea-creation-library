package com.jumpitt.aceptafealibrary;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by felipe on 16-02-18.
 */

class VerificationInteractor implements VerificationContract.Interactor {
    private VerificationContract.Presenter mListener;

    public VerificationInteractor(VerificationContract.Presenter mListener) {
        this.mListener = mListener;
    }

    @Override
    public void verifyPhone(String phone) {
        Call<CreateMessageResponse> send = RestClient.get().create(new CreateMessageBody("123", phone));
        send.enqueue(new Callback<CreateMessageResponse>() {
            @Override
            public void onResponse(Call<CreateMessageResponse> call, Response<CreateMessageResponse> response) {
                if (response.isSuccessful()) {
                    mListener.onPhoneSent(response.body());
                } else {
                    mListener.onPhoneSent(null);
                }
            }

            @Override
            public void onFailure(Call<CreateMessageResponse> call, Throwable t) {
                mListener.onPhoneSent(null);
            }
        });
    }

    @Override
    public void sendCode(String code, String oid) {
        Call<Void> verify = RestClient.get().verify(new VerifyMessageBody("123", oid, code));
        verify.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                mListener.onCodeSent(response.isSuccessful());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                mListener.onCodeSent(false);
            }
        });
    }
}
