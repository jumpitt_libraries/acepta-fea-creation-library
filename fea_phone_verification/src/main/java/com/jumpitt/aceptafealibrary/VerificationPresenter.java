package com.jumpitt.aceptafealibrary;

/**
 * Created by felipe on 16-02-18.
 */

class VerificationPresenter implements VerificationContract.Presenter {
    private VerificationContract.View listener;
    private VerificationInteractor interactor;
    private String oid;
    private boolean changeFragment = false;

    public VerificationPresenter(VerificationContract.View listener) {
        this.listener = listener;
        this.interactor = new VerificationInteractor(this);
    }

    @Override
    public void sendPhone(String phone, boolean changeFragment) {
        listener.showProgressDialog("Enviando...");
        this.changeFragment = changeFragment;
        interactor.verifyPhone(phone);
    }

    @Override
    public void onPhoneSent(CreateMessageResponse body) {
        listener.dismissProgressDialog();
        if (body != null) {
            oid = body.getOID();
            if (changeFragment)
                listener.onPhoneSuccess();
        } else {
            listener.showMessage("Error al enviar, reintente");
        }
    }

    @Override
    public void sendCode(String code) {
        listener.showProgressDialog("Validando...");
        interactor.sendCode(code, oid);
    }

    @Override
    public void onCodeSent(boolean successful) {
        listener.dismissProgressDialog();
        if (successful) {
            listener.onCodeSuccess();
        } else {
            listener.showMessage("Error al verificar, Reintente");
        }
    }
}
