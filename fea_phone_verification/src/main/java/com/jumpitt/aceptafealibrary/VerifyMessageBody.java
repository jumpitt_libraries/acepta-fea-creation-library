package com.jumpitt.aceptafealibrary;

import com.google.gson.annotations.SerializedName;

class VerifyMessageBody {
    @SerializedName("auth_token")
    private String token;
    private String oid;
    @SerializedName("totp")
    private String code;

    public VerifyMessageBody(String token, String oid, String code) {
        this.token = token;
        this.oid = oid;
        this.code = code;
    }
}
