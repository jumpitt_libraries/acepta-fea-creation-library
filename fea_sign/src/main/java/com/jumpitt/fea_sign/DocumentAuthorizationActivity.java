package com.jumpitt.fea_sign;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;

import java.io.IOException;
import java.io.InputStream;

import static com.jumpitt.fea_sign.DocumentAuthorizationPresenter.METHOD_FINGER;
import static com.jumpitt.fea_sign.DocumentAuthorizationPresenter.METHOD_PASSWORD;
import static com.jumpitt.fea_sign.Utils.retrieveStream;


public class DocumentAuthorizationActivity extends BaseActivity implements DocumentAuthorizationContract.View {
    public static final int SIGN_SUCCESS = 1001;
    public static final int REJECT_SUCCESS = 1001;
    public static final int REQUEST_SIGN = 312;
    public static final int REQUEST_SUCCESS = 320;
    public static final String NO_PREVIEW = "NO_PREVIEW";
    public static final String SOCKET_ID = "SOCKET_ID";
    public static final String DOCUMENT_ID = "DOCUMENT_ID";
    public static final String DOCUMENT_CODE = "DOCUMENT_CODE";
    public static final String DNI = "DNI";
    public static final String DOCUMENT_URL = "DOCUMENT_URL";
    public static final String DOCUMENT_INSTITUTION = "DOCUMENT_INSTITUTION";
    public static final String DOCUMENT_NAME = "DOCUMENT_NAME";
    public static final String DOCUMENT_CREATED_AT = "DOCUMENT_CREATED_AT";
    public static final String DOCUMENT_INSTITUTION_LOGO = "DOCUMENT_INSTITUTION_LOGO";
    public static final String DOCUMENT_STATE = "DOCUMENT_STATE";
    private static final int DIALOG_DISMISS_DELAY = 1000;
    private FEAFingerprintHelper mFeaFingerprintHelper;
    private FEASignDialog mSignDialog;
    private RegisterFingerDialog mRegisterFingerDialog;
    private DocumentAuthorizationContract.Presenter mPresenter;
    private String mID;
    private String mCode;
    private String mSocketID;
    private String mURL;
    private String mInstitution;
    private String mName;
    private String mDate;
    private String mLogo;

    private PDFView mPreview;
    private AppCompatButton showFullPreview;
    private ProgressBar progressBar;
    private AppBarLayout appBarLayout;
    private ImageView brandLogo;
    private TextView brandTitle;
    private TextView institution;
    private TextView title;
    private TextView date;
    private ImageView logo;
    private ImageView backButton;
    private CoordinatorLayout root;
    private AppCompatButton signButton;
    private AppCompatButton denyButton;
    private Drawable blackArrowDrawable;
    private Drawable whiteArrowDrawable;
    private CardView previewContainer;
    private TextView pageViewer;
    private int pageNumber = 0;
    private TextView pdfViewDocName;


    private MaterialDialog progressDialog;

    private InputStream inputStream;

    /* Intent config parameters */
    private String colorPrimaryHex;
    private String colorPrimaryDarkHex;
    private byte[] brandLogoBytes;
    private boolean darkMode = false;

    private boolean branded = false;
    private int type = -1;

    private boolean canSign = true;
    private boolean noPreview = false;
    private Toolbar toolbar;
    private ColorStateList buttonEnableColor;
    private ColorStateList buttonDisableColor;
    private FEAFingerprintHelper.OnFingerprintLister signListener = new FEAFingerprintHelper.OnFingerprintLister() {
        @Override
        public void onSuccess(String fea) {
            mSignDialog.update(true);
            if (mID != null)
                mPresenter.onSignRequest(fea, mID, mSocketID, type, METHOD_FINGER);
            else{
                mPresenter.onSignRequestDEC(fea, mCode, mDni, type, METHOD_FINGER);
            }
        }

        @Override
        public void onFail() {
            mSignDialog.update(false);
            Toast.makeText(DocumentAuthorizationActivity.this, "Fail", Toast.LENGTH_SHORT).show();
        }
    };
    private FEAFingerprintHelper.OnFingerprintLister registerListener = new FEAFingerprintHelper.OnFingerprintLister() {
        @Override
        public void onSuccess(String fea) {
            mRegisterFingerDialog.update(true);
            //Toast.makeText(DetailActivity.this, "Ahora puede firmar usando su huella", Toast.LENGTH_SHORT).show();
            createDialog(getString(R.string.fls_fingerprint_activated_dialog_title_text)
                    , getString(R.string.fls_fingerprint_activated_dialog_message_text)
                    , getString(R.string.fls_fingerprint_activated_dialog_positive_text)
                    , null)
                    .positiveColor(ContextCompat.getColor(getApplicationContext(), R.color.fls_fingerprint_activated_continue_text_color))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                tryToSignWithFinger(type);
                            }
                        }
                    }).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mRegisterFingerDialog.dismiss();
                }
            }, DIALOG_DISMISS_DELAY);
        }

        @Override
        public void onFail() {
            mRegisterFingerDialog.update(false);
            Toast.makeText(DocumentAuthorizationActivity.this, "Huella no reconocida", Toast.LENGTH_SHORT).show();
        }
    };
    private String mDni;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        branded = getIntent().getAction() != null;
        boolean hasExtras = extras != null;

        if (hasExtras && extras.containsKey(NO_PREVIEW)) {
            noPreview = extras.containsKey(NO_PREVIEW);
            setTheme(R.style.LibrarySignTheme_Transparent);
            Utils.convertActivityToTranslucent(this);
            //overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
        setContentView(R.layout.activity_detail);

        buttonDisableColor = ContextCompat.getColorStateList(this, R.color.fls_terms_and_conditions_text_disable_button_color);
        buttonEnableColor = ContextCompat.getColorStateList(this, R.color.fls_terms_and_conditions_text_enable_button_color);

        //mPreview = findViewById(R.id.preview); //PDF view
        mPreview = findViewById(R.id.pdf_tyc);
        //showFullPreview = findViewById(R.id.show_full_preview);
        progressBar = findViewById(R.id.progress_bar);
        appBarLayout = findViewById(R.id.appbar_parent);

        pageViewer = findViewById(R.id.pdf_view_page);
        pageViewer.setVisibility(View.GONE);

        //pdfViewDocName = findViewById(R.id.pdf_view_doc_name);
        brandLogo = findViewById(R.id.brand_logo);
        //brandTitle = findViewById(R.id.brand_title);

        toolbar = findViewById(R.id.toolbar);

        setupToolbar(toolbar);

        institution = findViewById(R.id.enterprise_text);
        //title = findViewById(R.id.title_text);
        title = findViewById(R.id.title_text_new);
        date = findViewById(R.id.time_text);
        logo = findViewById(R.id.enterprise_logo);

        backButton = findViewById(R.id.back_button);
        root = findViewById(R.id.root);
        signButton = findViewById(R.id.sign_button);
        denyButton = findViewById(R.id.deny_button);

        previewContainer = findViewById(R.id.card);

        blackArrowDrawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black);

        whiteArrowDrawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white);

        if (hasExtras) {
            mID = extras.getString(DOCUMENT_ID, null);
            mCode = extras.getString(DOCUMENT_CODE);
            mDni = extras.getString(DNI);
            mURL = extras.getString(DOCUMENT_URL);
            mInstitution = extras.getString(DOCUMENT_INSTITUTION);
            mName = extras.getString(DOCUMENT_NAME);
            mDate = extras.getString(DOCUMENT_CREATED_AT);
            mLogo = extras.getString(DOCUMENT_INSTITUTION_LOGO);
            canSign = extras.getString(DOCUMENT_STATE).equals("PENDIENTE");
            mSocketID = extras.getString(SOCKET_ID, null);
        } else {
            Intent intent = new Intent();
            intent.putExtra("DESCRIPTION", "Faltan párametros de configuración");
            setResult(RESULT_CANCELED, intent);
            finish();
        }

        toolbar.setTitle(mName != null ? mName : "Firma de Documento");
        //pdfViewDocName.setText(mName != null ? mName : "Documento sin nombre");

        if (mID == null && mCode == null) {
            Intent intent = new Intent();
            intent.putExtra("DESCRIPTION", "Falta parámetro TRANSACTION_ID");
            setResult(RESULT_CANCELED, intent);
            finish();
            return;
        }
        setupView();

        //showFullPreview.setVisibility(View.GONE);

        mPresenter = new DocumentAuthorizationPresenter(this);

        mFeaFingerprintHelper = FEAFingerprintHelper.getInstance();

        /*
        if (Hawk.contains(HAWK_NEVER_USER_FINGER)) {
            neverUseFinger = Hawk.get(HAWK_NEVER_USER_FINGER);
        }*/

        signButton.setVisibility(canSign ? View.VISIBLE : View.GONE);
        denyButton.setVisibility(canSign ? View.VISIBLE : View.GONE);


        /*backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });*/

        /*showFullPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent preview = new Intent(DocumentAuthorizationActivity.this, PreviewActivity.class);
                /*if (branded) {
                    preview.putExtra("BRAND_LOGO", brandLogoBytes);
                    preview.putExtra("DARK_MODE", darkMode);
                } // TODO:COLOCAR CIERRE DE COMENTARIO

                preview.putExtra("URL", mURL);
                startActivity(preview);
            }
        });*/

        signButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (FEAFingerprintHelper.isAvailable()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tryToSignWithFinger(FEASignDialog.TYPE_SIGN);
                    }
                } else {
                    signWithPassword(FEASignDialog.TYPE_SIGN);
                }
            }
        });

        denyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog(getString(R.string.fls_doc_authorization_deny_dialog_title_text)
                        , getString(R.string.fls_doc_authorization_deny_dialog_message_text)
                        , getString(R.string.fls_doc_authorization_deny_dialog_positive_option)
                        , getString(R.string.fls_doc_authorization_deny_dialog_negative_option))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if (FEAFingerprintHelper.isAvailable()) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        tryToSignWithFinger(FEASignDialog.TYPE_REJECT);
                                    }
                                } else {
                                    signWithPassword(FEASignDialog.TYPE_REJECT);
                                }
                            }
                        })
                        .show();
            }
        });
    }

    private void toggleTermsAndConditionsEnableButton(MaterialDialog dialog, boolean b) {
        dialog.getActionButton(DialogAction.POSITIVE).setEnabled(b);
        dialog.getActionButton(DialogAction.POSITIVE).setTextColor(b ? buttonEnableColor : buttonDisableColor);
    }

    private void setupView() {
        if (noPreview) {
            root.setBackgroundColor(ContextCompat.getColor(this, R.color.text_color_black_50op));
            findViewById(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
            //backButton.setVisibility(View.GONE);
            previewContainer.setVisibility(View.GONE);
            appBarLayout.setVisibility(View.GONE);
        } else {
            new getInputStreamFromUrl().execute(mURL);
        }

        if (institution != null)
            institution.setText(mInstitution);

        //if (mName != null)
        //title.setText(mName);
        /*if (mLogo != null) {
            if (mLogo.contains("http")) {
                Glide.with(this).load(mLogo).optionalCircleCrop().into(logo);
            } else {
                logo.setImageBitmap(Utils.bytesToBitmap(Base64.decode(mLogo, Base64.DEFAULT)));
            }
        }*/
        //if (mDate != null && date != null)
        //    date.setText(mDate);
    }

    private void signWithPassword(int type) {
        Intent fea = new Intent(this, FEASignActivity.class);
        fea.putExtra("TYPE", type);
        startActivityForResult(fea, REQUEST_SIGN);
    }

    private void tryToSignWithFinger(final int TYPE) {
        this.type = TYPE;
        if (FEAFingerprintHelper.isNeverUserFinger()) {
            signWithPassword(TYPE);
            return;
        }
        if (mFeaFingerprintHelper.isActivated()) {
            showSignDialog(TYPE);
            mFeaFingerprintHelper.setListener(signListener);
            mFeaFingerprintHelper.startAuth(null);
        } else {
            MaterialDialog.Builder builder = createDialog(getString(R.string.fls_try_sing_message_title)
                    , getString(R.string.fls_try_sing_message_content)
                    , getString(R.string.fls_try_sing_message_positive)
                    , getString(R.string.fls_try_sing_message_negative));
            builder
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            showTermsAndConditions();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            FEAFingerprintHelper.setNeverUserFinger(dialog.isPromptCheckBoxChecked());
                            signWithPassword(TYPE);
                        }
                    })
                    .checkBoxPrompt(getString(R.string.fls_try_sing_checkbox_message), false, new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            //Ignore
                        }
                    })
                    .widgetColor(ContextCompat.getColor(this, R.color.fls_activate_fingerprint_message_button_checkbox))
                    .build();

            builder.show();
        }
    }

    private void showTermsAndConditions() {
        mFeaFingerprintHelper.setListener(registerListener);

        View customContent = getLayoutInflater().inflate(R.layout.dialog_terms_and_conditions_custom, null);
        final TextInputEditText passTermsAndConditionsEditText = customContent.findViewById(R.id.sing_pass_input_edit_text);

        final MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .typeface(ResourcesCompat.getFont(this, R.font.bold), ResourcesCompat.getFont(this, R.font.regular))
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .title(getString(R.string.fls_terms_and_conditions_dialog_title_text))
                .titleColor(ContextCompat.getColor(this, R.color.fls_activate_sing_material_dialog_title_color))
                .customView(customContent, false)
                .negativeText(getString(R.string.fls_terms_and_conditions_dialog_cancel_button_text))
                .negativeColor(ContextCompat.getColor(this, R.color.fls_terms_and_conditions_text_enable_button_color))
                .positiveText(getString(R.string.fls_terms_and_conditions_dialog_accept_button_text));

        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                String input = passTermsAndConditionsEditText.getText().toString();
                showFingerprintDialog();
                mFeaFingerprintHelper.startAuth(input);
            }
        });

        final MaterialDialog dialog = builder.build();

        toggleTermsAndConditionsEnableButton(dialog, false);

        passTermsAndConditionsEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                toggleTermsAndConditionsEnableButton(dialog, true);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();

    }

    private void showFingerprintDialog() {
        if (mRegisterFingerDialog == null) {
            mRegisterFingerDialog = RegisterFingerDialog.newInstance();
        }
        mRegisterFingerDialog.show(getSupportFragmentManager(), "Register");
    }

    private void showSignDialog(int type) {
        if (mSignDialog == null) {
            mSignDialog = FEASignDialog.newInstance(type, mName, mInstitution);
        }
        mSignDialog.show(getSupportFragmentManager(), "Sign");
    }

    @Override
    public void onUsePasswordPressed(int TYPE) {
        this.type = TYPE;
        mFeaFingerprintHelper.setActivated(false);
        signWithPassword(type);
    }

    @Override
    public void onSignSuccess() {
        showSuccess();
    }

    @Override
    public void showLoadingDialog() {
        if (progressDialog == null) progressDialog = new MaterialDialog.Builder(this)
                .theme(Theme.LIGHT)
                .progress(true, 100)
                .widgetColor(ContextCompat.getColor(this, R.color.fls_sing_pass_document_loading_progress_bar_color))
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .content("Cargando...")
                .build();
        progressDialog.show();
    }

    @Override
    public void dismissLoading() {
        if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
    }

    @Override
    public void onSignError(String glosa) {
        Toast.makeText(this, (glosa != null) ? glosa : "Ha ocurrido un error. REINTENTE", Toast.LENGTH_SHORT).show();
    }

    //-----Fingerpirnt helper Listeners----

    @Override
    public void onDialogDismiss() {
        mFeaFingerprintHelper.stopAuth();
    }

    private MaterialDialog.Builder createDialog(String title, String content, String positive, String negative) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .typeface(ResourcesCompat.getFont(this, R.font.bold), ResourcesCompat.getFont(this, R.font.regular))
                .negativeColor(getResources().getColor(R.color.text_color_black))
                .cancelable(false)
                .canceledOnTouchOutside(false);
        if (title != null) {
            builder.title(title);
            builder.titleColor(ContextCompat.getColor(this, R.color.fls_activate_sing_material_dialog_title_color));
        }
        if (content != null)
            builder.content(content);
        if (negative != null) {
            builder.negativeText(negative);
            builder.negativeColor(ContextCompat.getColor(this, R.color.fls_activate_fingerprint_message_button_negative));
        }
        if (positive != null) {
            builder.positiveText(positive);
            builder.positiveColor(ContextCompat.getColor(this, R.color.fls_activate_fingerprint_message_button_positive));
        }

        return builder;
    }

    private void showSuccess() {
        Intent intent = new Intent(getApplicationContext(), SignSuccessActivity.class);
        intent.putExtra(SignSuccessActivity.EXTRA_LOGO, (type == 0) ? R.drawable.ic_sign_success_logo : R.drawable.ic_sign_reject_logo);
        intent.putExtra(SignSuccessActivity.EXTRA_TITLE, type == 0 ? getString(R.string.fls_sign_document_success_title) : getString(R.string.fls_reject_document_success_title));
        intent.putExtra(SignSuccessActivity.EXTRA_MESSAGE, type == 0 ? getString(R.string.fls_sign_document_success_message) : getString(R.string.fls_reject_document_success_message));
        intent.putExtra(SignSuccessActivity.EXTRA_BUTTON_TEXT, getString(R.string.fls_success_next_button_text));
        intent.putExtra(SignSuccessActivity.EXTRA_TYPE, SignSuccessActivity.TYPE_SIGN);
        startActivityForResult(intent, REQUEST_SUCCESS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SIGN) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    String fea = extras.getString("FEA");
                    type = extras.getInt("TYPE");
                    if(mID != null) {
                        mPresenter.onSignRequest(fea, mID, mSocketID, type, METHOD_PASSWORD);
                    }else{
                        mPresenter.onSignRequestDEC(fea, mCode, mDni, type, METHOD_PASSWORD);
                    }
                } else {
                    Toast.makeText(this, "Proceso cancelado", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Proceso cancelado", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == REQUEST_SUCCESS) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //if (noPreview)
        //    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @SuppressLint("StaticFieldLeak")
    private class getInputStreamFromUrl extends AsyncTask<String, Object, InputStream> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected InputStream doInBackground(String... data) {
            inputStream = retrieveStream(data[0]);
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream InputStream) {
            mPreview.fromStream(inputStream)
                    .defaultPage(pageNumber)
                    .onPageChange(new OnPageChangeListener() {
                        @Override
                        public void onPageChanged(int page, int pageCount) {
                            pageNumber = page;
                            pageViewer.setText(String.format("%s / %s", page + 1, pageCount));
                            pageViewer.setVisibility(View.VISIBLE);
                        }
                    })
                    .onLoad(new OnLoadCompleteListener() {
                        @Override
                        public void loadComplete(int nbPages) {
                            //showFullPreview.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    })
                    .enableAnnotationRendering(true)
                    .load();
            //mPreview.zoomTo(1.6f);
        }
    }
}
