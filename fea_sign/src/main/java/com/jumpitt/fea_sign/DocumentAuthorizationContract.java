package com.jumpitt.fea_sign;


interface DocumentAuthorizationContract {
    interface View {

        void onUsePasswordPressed(int TYPE);

        void onSignSuccess();

        void showLoadingDialog();

        void dismissLoading();

        void onSignError(String glosa);

        void onDialogDismiss();
    }

    interface Presenter {

        void onSignRequest(String password, String document, String socketID, int type, String method);

        void onSignRequestDEC(String password, String document, String dni, int type, String method);

    }
}
