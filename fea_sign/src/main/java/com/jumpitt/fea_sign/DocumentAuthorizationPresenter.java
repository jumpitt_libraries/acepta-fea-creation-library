package com.jumpitt.fea_sign;

import com.jumpitt.fea_library.FEAClient;
import com.jumpitt.fea_library.FEAListener;
import com.jumpitt.fea_library.model.SignResponse;

import static com.jumpitt.fea_sign.FEASignDialog.TYPE_SIGN;

class DocumentAuthorizationPresenter implements DocumentAuthorizationContract.Presenter {
    public static final String METHOD_FINGER = "fingerprint";
    public static final String METHOD_PASSWORD = "password";
    private FEAClient mFEAClient;

    private DocumentAuthorizationContract.View mListener;
    private FEAListener<SignResponse> signListener = new FEAListener<SignResponse>() {
        @Override
        public void onSuccess(SignResponse baseResponse) {
            mListener.dismissLoading();
            mListener.onSignSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            mListener.dismissLoading();
            mListener.onSignError(throwable.getCause().getMessage());

        }
    };

    public DocumentAuthorizationPresenter(DocumentAuthorizationContract.View mListener) {
        this.mListener = mListener;
        this.mFEAClient = FEAClient.getInstance();
    }

    @Override
    public void onSignRequest(String password, String document, String socketId, int type, String method) {
        mListener.showLoadingDialog();

        //mListener.onSignSuccess();
        //mListener.dismissLoading();

        if (type == TYPE_SIGN) {
            mFEAClient.signTransaction(document, socketId, password, signListener);
        } else {
            mFEAClient.rejectTransaction(document, socketId, password, signListener);
        }
    }

    @Override
    public void onSignRequestDEC(String password, String document, String dni, int type, String method) {
        mListener.showLoadingDialog();

        //mListener.onSignSuccess();
        //mListener.dismissLoading();

        if (type == TYPE_SIGN) {
            mFEAClient.signTransactionDEC(document, dni, password, signListener);
        } else {
            mFEAClient.rejectTransactionDEC(document, dni, password, signListener);
        }
    }
}
