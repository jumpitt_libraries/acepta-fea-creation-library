package com.jumpitt.fea_sign;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.KeyStore;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

@TargetApi(Build.VERSION_CODES.M)
public class FEAFingerprintHelper extends FingerprintManager.AuthenticationCallback {
    private static final String PREFERENCES_NAME = "FEA_FINGERPRINT_PREFERENCES";
    private static final int PREFERENCES_MODE = Context.MODE_PRIVATE;
    public static final String HAWK_MESSAGE_ENCRYPT = "hawk_message_encrypted";
    public static final String HAWK_IV = "hawk_iv";
    private static final String TAG = "FingerprintHelper";
    private static final String KEY_NAME = "FEA";

    private static final String CIPHER_INSTANCE = KeyProperties.KEY_ALGORITHM_AES + "/"
            + KeyProperties.BLOCK_MODE_CBC + "/"
            + KeyProperties.ENCRYPTION_PADDING_PKCS7;
    private final SharedPreferences sharedPreferences;
    private String mPassword;

    private boolean op;
    private String IV;

    private Context mContext;
    private static FingerprintManager mFingerprintManager;
    private static KeyStore mKeyStore;
    private static Cipher mCipher;
    private CancellationSignal mCancellationSignal;
    private OnFingerprintLister mListener;

    private static boolean available = false;
    private static boolean activated = false;
    private static boolean neverUserFinger = false;
    private static String cause = "";
    private static FEAFingerprintHelper mHelper;


    public FEAFingerprintHelper(Context mContext) {
        this.mContext = mContext;
        this.sharedPreferences = mContext.getSharedPreferences(PREFERENCES_NAME, PREFERENCES_MODE);
    }

    public static FEAFingerprintHelper getInstance() {
        return mHelper;
    }

    public static void init(Context mContext) {
        mHelper = new FEAFingerprintHelper(mContext);

        KeyguardManager keyguardManager = (KeyguardManager) mContext.getSystemService(Context.KEYGUARD_SERVICE);
        if (!keyguardManager.isKeyguardSecure()) {
            cause = "Lock screen security not enabled in Settings";
            Log.e(TAG, cause);
            //Toast.makeText(mContext, cause, Toast.LENGTH_SHORT).show();
            return;
        }

        if (mContext.checkSelfPermission(Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            cause = "No tiene permisos para utilizar autenticación por huella digital";
            Log.e(TAG, cause);
            //Toast.makeText(mContext, cause, Toast.LENGTH_SHORT).show();
            return;
        }
        mFingerprintManager = (FingerprintManager) mContext.getSystemService(Context.FINGERPRINT_SERVICE);
        if (!mFingerprintManager.isHardwareDetected()) {
            cause = "Sensor de huella digital no encontrado";
            Log.e(TAG, cause);
            //Toast.makeText(mContext, cause, Toast.LENGTH_SHORT).show();
            return;
        }
        if (!mFingerprintManager.hasEnrolledFingerprints()) {
            // This happens when no fingerprints are registered.
            cause = "Debe tener al menos una huella registrada para continuar";
            Log.e(TAG, cause);
            //Toast.makeText(mContext, cause, Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            mKeyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            Log.e(TAG, "KeyStore init error", e);
            return;
        }

        try {
            Log.d(TAG, "Cipher init success");
            mCipher = Cipher.getInstance(CIPHER_INSTANCE);
        } catch (Exception e) {
            Log.e(TAG, "Cipher init error", e);
            return;
        }

        generateKey();

        available = true;
    }

    private static void generateKey() {
        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        try {
            mKeyStore.load(null); //Load default config
            if (mKeyStore.containsAlias(KEY_NAME)) {
                return;
            }
            keyGenerator.init(new KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean initCipher() {
        try {
            mKeyStore.load(null);
            SecretKey key = (SecretKey) mKeyStore.getKey(KEY_NAME, null);
            if (op) {
                mCipher.init(Cipher.ENCRYPT_MODE, key);
                IV = Base64.encodeToString(mCipher.getIV(), Base64.URL_SAFE);
            } else {
                IV = sharedPreferences.getString(HAWK_IV, null);
                mCipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(Base64.decode(IV, Base64.URL_SAFE)));
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * FingerprintManager actions callback
     */
    private FingerprintManager.AuthenticationCallback mCallback = new FingerprintManager.AuthenticationCallback() {
        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            if (errMsgId == 7 && mListener != null) {
                Log.e(TAG, errString.toString());
                //Toast.makeText(mContext, errString, Toast.LENGTH_LONG).show();
                mListener.onFail();
            }
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            if (mListener == null) return;
            if (helpMsgId != 5) {
                Log.e(TAG, helpString.toString());
                //Toast.makeText(mContext, helpString, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onAuthenticationFailed() {
            if (mListener != null) mListener.onFail();
            Log.i(TAG, "Huella no reconocida");
            //Toast.makeText(mContext, "Huella no reconocida", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            if (!op) {
                retrievePassword();
            } else {
                storePassword();
            }
        }
    };

    private void retrievePassword() {
        try {
            if (!sharedPreferences.contains(HAWK_MESSAGE_ENCRYPT)) {
                return;
            }
            String message = sharedPreferences.getString(HAWK_MESSAGE_ENCRYPT, null);
            byte[] data = mCipher.doFinal(Base64.decode(message, Base64.URL_SAFE));
            String dec = new String(data, "UTF-8");

            if (mListener != null)
                mListener.onSuccess(dec);
        } catch (BadPaddingException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            Log.e(TAG, "Error al recuperar la clave", e);
            if (mListener != null) mListener.onFail();
        }
    }

    private void storePassword() {
        try {
            byte[] encrypted = mCipher.doFinal(mPassword.getBytes());

            String message = Base64.encodeToString(encrypted, Base64.URL_SAFE);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(HAWK_MESSAGE_ENCRYPT, message);
            editor.putString(HAWK_IV, IV);
            editor.apply();

            if (mListener != null)
                mListener.onSuccess(null);

            activated = true;
            Log.d(TAG, "Finger mask success");
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            Log.e(TAG, "No se pudo almacenar la clave", e);
            if (mListener != null) mListener.onFail();
        }

    }

    public static boolean isAvailable() {
        return available;
    }

    public String getCause() {
        return cause;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean active) {
        activated = active;
    }

    /**
     * Start finger check for store or get FEA on depend of {@param fea} value
     *
     * @param fea if null get else store
     */
    public void startAuth(String fea) {
        this.op = (fea != null);
        this.mPassword = fea;

        Log.d(TAG, "STARTING " + (op ? "STORE" : "RETRIEVE"));
        Log.d(TAG, "Waiting for finger read...");
        if (initCipher()) {
            mCancellationSignal = new CancellationSignal();
            if (mContext.checkSelfPermission(Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(mContext, "No tiene permisos para utilizar autenticación por huella digital", Toast.LENGTH_SHORT).show();
                return;
            }
            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(mCipher);
            mFingerprintManager.authenticate(cryptoObject, mCancellationSignal, 0, mCallback, null);
        }
    }

    public void stopAuth() {
        Log.d(TAG, "Finger read cancelled");
        if (mCancellationSignal != null) {
            mCancellationSignal.cancel();
            mCancellationSignal = null;
        }
    }

    public void setListener(OnFingerprintLister listener) {
        this.mListener = listener;
    }

    public void removeListener() {
        mListener = null;
    }

    public static boolean isNeverUserFinger() {
        return neverUserFinger;
    }

    public static void setNeverUserFinger(boolean neverUserFinger) {
        FEAFingerprintHelper.neverUserFinger = neverUserFinger;
    }

    public interface OnFingerprintLister {
        void onSuccess(String fea);

        void onFail();
    }
}
