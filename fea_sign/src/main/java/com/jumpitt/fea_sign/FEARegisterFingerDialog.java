package com.jumpitt.fea_sign;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

class FEARegisterFingerDialog extends BottomSheetDialogFragment implements FEAFingerprintHelper.OnFingerprintLister {

    //private DetailContract.View mListener;
    private ImageView image;
    private Drawable icDefault;
    private Drawable icSuccess;
    private FEAFingerprintHelper mHelper;
    private boolean registeredFlag = false;
    private static OnFingerprintRegisterListener listener;

    public static FEARegisterFingerDialog newInstance(@NonNull OnFingerprintRegisterListener registerListener) {
        listener = registerListener;
        Bundle args = new Bundle();
        FEARegisterFingerDialog fragment = new FEARegisterFingerDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_register_fingerprint, container, false);
        //if (getActivity() != null)
        //mListener = (DetailContract.View) getActivity();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        image = view.findViewById(R.id.status_image);
        icDefault = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_fingerprint_default);
        icSuccess = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_fingerprint_success);

        view.findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCancelPressed();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHelper = FEAFingerprintHelper.getInstance();
        mHelper.setListener(this);
        mHelper.startAuth("FEA");
    }

    void onCancelPressed() {
        mHelper.stopAuth();
        mHelper.removeListener();
        dismissAllowingStateLoss();
    }

    private void update(boolean success) {
        registeredFlag = success;
        image.setImageDrawable((success) ? icSuccess : icDefault);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        listener.registered(registeredFlag);
        mHelper = null;
    }

    @Override
    public void onSuccess(String fea) {
        //Toast.makeText(DetailActivity.this, "Ahora puede firmar usando su huella", Toast.LENGTH_SHORT).show();
        //createDialog("Huella Activada", "Ahora puede firmar usando la huella registrada en su teléfono", "Entendido", null).show();
        update(true);
        //Toast.makeText(getView().getContext(), "Ahora puede firmar usando la huella registrada en su teléfono", Toast.LENGTH_LONG).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, 1000);
    }

    @Override
    public void onFail() {
        update(false);
        //Toast.makeText(getView().getContext(), "Huella no reconocida", Toast.LENGTH_SHORT).show();
    }


    public interface OnFingerprintRegisterListener {
        void registered(boolean status);
    }
}
