package com.jumpitt.fea_sign;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static com.jumpitt.fea_sign.FEASignDialog.TYPE_SIGN;

public class FEASignActivity extends BaseActivity {

    private Toolbar toolbar;
    private EditText input;
    private MaterialButton okButton;
    private ColorStateList buttonEnableColor;
    private ColorStateList buttonDisableColor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_use_fea);

        toolbar = findViewById(R.id.toolbar_use_fea);
        input = findViewById(R.id.sing_pass_input_edit_text);
        okButton = findViewById(R.id.ok_button);

        buttonDisableColor = ContextCompat.getColorStateList(this, R.color.fls_pass_sing_ok_button_disable_color);
        buttonEnableColor = ContextCompat.getColorStateList(this, R.color.fls_pass_sing_ok_button_color);

        toggleButtonEnable(false);

        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.fls_sing_pass_toolbar_title));

        if (getActionBar() != null) {
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().getExtras() != null) {
            int type = getIntent().getExtras().getInt("TYPE");
            okButton.setText((type == TYPE_SIGN) ? "Firmar Documento" : "Denegar Documento");
        }

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOkButtonPressed();
            }
        });

        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onInputTextChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void toggleButtonEnable(boolean b) {
        if (okButton == null) return;
        okButton.setBackgroundTintList(b ? buttonEnableColor : buttonDisableColor);
        okButton.setEnabled(b);
    }

    void onOkButtonPressed() {
        Intent data = new Intent();
        data.putExtra("FEA", input.getText().toString());
        data.putExtras(getIntent());
        setResult(RESULT_OK, data);
        finish();
    }

    void onInputTextChanged() {
        if (input.getText().toString().isEmpty()) {
            toggleButtonEnable(false);
        } else {
            toggleButtonEnable(true);
        }
    }
}
