package com.jumpitt.fea_sign;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

public class FEASignDialog extends BottomSheetDialogFragment {
    private static final String TYPE_ARG = "TYPE_ARG";
    private static final String NAME_ARG = "NAME_ARG";
    private static final String INSTITUTION_ARG = "INSTITUTION_ARG";
    public static final int TYPE_SIGN = 0;
    public static final int TYPE_REJECT = 1;

    private int TYPE;
    private ImageView image;
    private Drawable icDefault;
    private Drawable icSuccess;
    private DocumentAuthorizationContract.View mListener;

    public static FEASignDialog newInstance(@NonNull int type, @NonNull String name, @NonNull String institution) {
        Bundle args = new Bundle();
        args.putInt(TYPE_ARG, type);
        args.putString(NAME_ARG, name);
        args.putString(INSTITUTION_ARG, institution);
        FEASignDialog fragment = new FEASignDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_sign_with_fingerprint, container, false);

        image = view.findViewById(R.id.status_image);
        TextView description = view.findViewById(R.id.description);
        TextView institutionLabel = view.findViewById(R.id.institution);
        TextView documentName = view.findViewById(R.id.document_name);
        icDefault = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_fingerprint_default);
        icSuccess = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_fingerprint_success);

        view.findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCancelPressed();
            }
        });

        view.findViewById(R.id.pass_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPassPressed();
            }
        });

        assert getArguments() != null;
        TYPE = getArguments().getInt(TYPE_ARG);
        String name = getArguments().getString(NAME_ARG);
        String institution = getArguments().getString(INSTITUTION_ARG);

        institutionLabel.setText(institution);
        documentName.setText(name);

        description.setText(String.format(Locale.getDefault(), "Pon tu dedo en el sensor para %s", ((TYPE == TYPE_SIGN) ? "firmar" : "rechazar")));
        if (getActivity() != null)
            mListener = (DocumentAuthorizationContract.View) getActivity();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener.onDialogDismiss();
    }

    void onCancelPressed() {
        dismissAllowingStateLoss();
    }

    void onPassPressed() {
        dismissAllowingStateLoss();
        if (mListener != null)
            mListener.onUsePasswordPressed(TYPE);
    }

    public void update(boolean success) {
        image.setImageDrawable((success) ? icSuccess : icDefault);
    }

}
