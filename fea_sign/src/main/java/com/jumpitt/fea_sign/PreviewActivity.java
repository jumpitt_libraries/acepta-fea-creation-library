package com.jumpitt.fea_sign;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class PreviewActivity extends BaseActivity {
    ImageView brandLogo;
    PDFView mPreview;

    private InputStream mStream;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        brandLogo = findViewById(R.id.brand_logo);
        mPreview = findViewById(R.id.pdf_view);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String mURL = extras.getString("URL");
            new getInputStreamFromUrl().execute(mURL);
        }

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private class getInputStreamFromUrl extends AsyncTask<String, Object, InputStream> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected InputStream doInBackground(String... data) {
            try {
                return new URL(data[0]).openStream();
            } catch (IOException e) {
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            if (inputStream != null) {
                mStream = inputStream;
                mPreview.fromStream(inputStream)
                        .defaultPage(0)
                        .enableAnnotationRendering(true)
                        .load();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mStream != null) {
            try {
                mStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
