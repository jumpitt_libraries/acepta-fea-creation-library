package com.jumpitt.fea_sign;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class RegisterFingerDialog extends BottomSheetDialogFragment {

    private DocumentAuthorizationContract.View mListener;

    ImageView image;
    Drawable icDefault;
    Drawable icSuccess;

    public static RegisterFingerDialog newInstance() {
        Bundle args = new Bundle();
        RegisterFingerDialog fragment = new RegisterFingerDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_register_fingerprint, container, false);

        if (getActivity() != null)
            mListener = (DocumentAuthorizationContract.View) getActivity();


        image = view.findViewById(R.id.status_image);
        icDefault = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_fingerprint_default);
        icSuccess = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_fingerprint_success);

        view.findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCancelPressed();
            }
        });
        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener.onDialogDismiss();
    }

    void onCancelPressed() {
        dismissAllowingStateLoss();
    }


    public void update(boolean success) {
        image.setImageDrawable((success) ? icSuccess : icDefault);
    }

}
