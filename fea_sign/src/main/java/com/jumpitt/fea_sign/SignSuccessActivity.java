package com.jumpitt.fea_sign;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by felipe on 01-02-18.
 */

public class SignSuccessActivity extends AppCompatActivity {
    public static final String EXTRA_LOGO = "extraLogo";
    public static final String EXTRA_TITLE = "extraTitle";
    public static final String EXTRA_MESSAGE = "extraMessage";
    public static final String EXTRA_BUTTON_TEXT = "extraButtonText";
    public static final String EXTRA_TYPE = "extraType";
    public static final int TYPE_SIGN = 1003;

    ConstraintLayout root;
    ImageView logo;
    AppCompatTextView title;
    AppCompatTextView message;
    AppCompatButton button;
    int type;
    String phone;
    private final String TAG = SignSuccessActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);


        root = findViewById(R.id.sign_container);
        logo = findViewById(R.id.logo);
        title = findViewById(R.id.title);
        message = findViewById(R.id.message);
        button = findViewById(R.id.continue_button);

        Log.i(TAG, "Entro a success de Creation");

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int drawable = extras.getInt(EXTRA_LOGO);
            logo.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawable, null));
            title.setText(extras.getString(EXTRA_TITLE));
            message.setText(extras.getString(EXTRA_MESSAGE));
            button.setText(extras.getString(EXTRA_BUTTON_TEXT));
            type = extras.getInt(EXTRA_TYPE);
            phone = extras.getString("PHONE");
        }

        findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onContinueButtonClicked();
            }
        });
    }

    void onContinueButtonClicked() {
        switch (type) {
            case TYPE_SIGN:
                setResult(RESULT_OK);
                finish();
                break;
            default:
                Log.w(getString(R.string.app_name), "Unknown Success Type");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        switch (type) {
            case TYPE_SIGN:
                setResult(RESULT_OK);
                super.onBackPressed();
                break;
            default:
                new AlertDialog.Builder(this)
                        .setTitle("¿Desea Salir?")
                        .setMessage("Si sale perderá todo el progreso.")
                        .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
                break;
        }
    }
}
